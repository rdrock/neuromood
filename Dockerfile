# Base Image
FROM python3.8-alpine3.11

# Set working directory
WORKDIR /usr/src/app

# Set environment variables
ENV PORT=5000

# Install Dependencies
RUN pip install --upgrade pip
COPY ./requires/installation.txt /usr/src/app/requires/installation.txt
RUN pip install -r requires/installation.txt

# Copy app
COPY . /usr/src/app/

# Run application
CMD ["flask run"]
