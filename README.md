# NeuroMood

Track and rate your moods in order to best align a path to happiness 😄

## Requirements

- Python3+

## Environment Variables

### Required

| Variable                       | Description                                                                           | Default       |
| ------------------------------ | ------------------------------------------------------------------------------------- | ------------- |
| `DATABASE`                     | The name of the database                                                              | `neuromood`   |
| `FLASK_ENV`                    | Indicates current running environment                                                 | `production`  |
| `SECRET_KEY`                   | Used to securely sign session cookie and other security needs                         | `None`        |
| `SESSION_COOKIE_NAME`          | Name of session cookie                                                                | `session`     |
| `SESSION_COOKIE_PATH`          | The path the session cookie will be valid for                                         | `None`        |
| `SESSION_COOKIE_HTTPONLY`      | Browsers will not allow JavaScript access to cookies marked as `HTTP only`            | `True`        |
| `SESSION_COOKIE_SECURE`        | Browsers only send cookies with requests over HTTPS if cookie marked `secure`         | `False`       |
| `SESSION_REFRESH_EACH_REQUEST` | Flag to control if cookie is sent with every request when `session.permanent` is true | `True`        |
| `SQLALCHEMY_DATABASE_URI`      | Database URI to use for connection                                                    | `sqlite:////` |

### Optional

| Variable                         | Description                                                              | Default                           |
| -------------------------------- | ------------------------------------------------------------------------ | --------------------------------- |
| `DEBUG`                          | Enables debugger, and server reloads when code changes                   | `True` IFF `ENV` is `development` |
| `TESTING`                        | Enables tesing mode. Exceptions are propagated                           | `False`                           |
| `SQLALCHEMY_ECHO`                | Flag to indicate SQLAlchemy should log all statements issued to `stderr` | `False`                           |
| `SQLALCHEMY_RECORD_QUERIES`      | Flag to indicate query recording. Defaulted in debug or testing modes    | IFF `DEBUG` or `TESTING` mode     |
| `SQLALCHEMY_TRACK_MODIFICATIONS` | Flag to indicate tracking of objects and emit signals, if `True`         | `None`                            |

## Development

```bash
    git clone https://gitlab.com/rdrock/neuromood.git
    cd neuromood
    python3 -m venv venv
    . venv/bin/activate
    pip install -r requires/development.txt
```

## Testing

**Note:** Currently Not Complete

```bash
    pytest
```

## Running Application

**Note:** You should be able to run the application with only `flask run`, but there seems to be some odd collision issues when Python2.7 is present. Please refer to this [Stackoverflow](https://stackoverflow.com/questions/47005658/importerror-no-module-named-flask-login-even-though-i-have-it-installed).

```bash
    python3 -m flask init-db
    python3 -m flask run
```
