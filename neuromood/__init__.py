"""Initialize Application"""
import base64
import os

from flask import Flask
from flask_json import FlaskJSON, JsonError, json_response, as_json
from flask_login import LoginManager

from neuromood.database import db

login_manager = LoginManager()


def create_app(test_config=None):
    """Construct application"""
    app = Flask(__name__, instance_relative_config=True)

    # App Configuration
    if test_config is None:
        # Load instance config, IFF exists, when not testing
        app.config.from_pyfile('settings.cfg', silent=False)
    else:
        # Load test config, if passed in
        app.config.from_mapping(test_config)

    SECRET_KEY = app.config['SECRET_KEY']
    if not SECRET_KEY:
        raise ValueError('No SECRET_KEY set for Flask application')

    app.config.from_mapping(SECRET_KEY=SECRET_KEY,
                            DATABASE=os.path.join(app.instance_path,
                                                  'neuromood.sqlite'))

    # Ensure instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # Initialize JSON extension
    json = FlaskJSON()
    json.init_app(app)

    with app.app_context():
        # Register database and CLI commands
        db.init_app(app)

        # Import models before initializing
        from neuromood.models.user import User
        from neuromood.models.mood import Mood

        # Create Database Models
        db.create_all()

        login_manager.init_app(app)

        # Register Blueprints
        from neuromood.auth import auth_bp
        from neuromood.blueprints.mood import mood_bp
        from neuromood.blueprints.user import user_bp
        app.register_blueprint(auth_bp)
        app.register_blueprint(mood_bp)
        app.register_blueprint(user_bp)

    @login_manager.user_loader
    def user_loader(user_id):
        return User.query.filter_by(id=user_id).first()

    @login_manager.request_loader
    def request_loader(request):
        # First, try login with api_key url arg
        api_key = request.args.get('api_key')
        if api_key:
            user = User.query.filter_by(api_key=api_key).first()
            if user:
                return user

        # Second, try login using Basic Auth
        api_key = request.headers.get('Authorization')
        if api_key:
            api_key = api_key.replace('Basic', '', 1)
            try:
                api_key = base64.b64decode(api_key)
            except TypeError:
                pass

            user = User.query.filter_by(api_key=api_key).first()
            if user:
                return user

        # Finally, return None if neither worked
        return None

    @login_manager.unauthorized_handler
    def unauthorized_handler():
        return 'Unauthorized'

    # Routing
    @app.route('/')
    def index():
        return 'NeuroMood Home'

    return app
