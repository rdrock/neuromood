import functools

from flask import Blueprint, g, request

from flask_json import as_json
import flask_login

from neuromood.database import db
from neuromood.models.user import User

auth_bp = Blueprint('auth', __name__, url_prefix='/auth')


@auth_bp.before_app_request
def load_logged_in_user():
    """If an authenticated user is stored in the session, load them
    the user from the database to ``g.user``
    """
    curr_user = flask_login.current_user.get_id()
    if curr_user:
        g.user = User.query.filter_by(id=curr_user).first()
    else:
        g.user = None


@auth_bp.route('/login', methods=['POST'])
@as_json
def login():
    err = None
    req_json = request.get_json()
    email = req_json['email']
    pwd = req_json['password']

    user = User.query.filter_by(email=email).first()

    if user is None:
        err = "Incorrect email"
    elif pwd != user.password:
        err = "Incorrect password"

    if err is None:
        flask_login.login_user(user)

        return dict({'message': 'Logged in'})

    return dict({'message': 'Invalid login'})


@auth_bp.route('/signup', methods=['POST'])
@as_json
def signup():
    """Create a new user account"""
    err = None
    req = request.get_json()
    email = req['email']
    pwd = req['password']

    if not email:
        err = 'Email is required.'
    elif not pwd:
        err = 'Password is required.'
    elif (User.query.filter_by(email=email).first() is not None):
        err = f'User with email {email} already exists'

    if err is None:
        # Email is not registered, able to create
        usr = User(email=email, password=pwd, streak=0)
        flask_login.login_user(usr)

        return dict({'message': 'User created'}), 201


@auth_bp.route('/logout', methods=['POST'])
@flask_login.login_required
def logout():
    flask_login.logout_user()
    return dict({'message': 'Logged Out'}), 200


@auth_bp.route('/protected')
@flask_login.login_required
def protected():
    return 'Logged in as: {}'.format(flask_login.current_user)
