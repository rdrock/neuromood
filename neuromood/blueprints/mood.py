"""Routes for interacting with Moods"""
from datetime import datetime

from flask import Blueprint, request
from flask_json import as_json
from flask_login import login_required, current_user

import neuromood.services as services
from neuromood.database import db
from neuromood.models.mood import Mood
from neuromood.models.user import User

mood_bp = Blueprint('mood', __name__)


@mood_bp.route('/mood', methods=['POST'])
@login_required
@as_json
def create_mood_rating():
    """Create a new mood entry for the currently logged in user"""
    try:
        user_id = current_user.get_id()
        if user_id is None:
            raise AttributeError("No current user id")

        prev_entry = services.get_last_insertion_date(user_id=user_id)

        # Verify if within streak time frame
        is_in_timeframe = services.is_within_steak_time_frame(
            prev_entry[0]['mood_created_date'])
        if not is_in_timeframe:
            services.reset_user_streak(user_id=user_id)
            print('Resetting User streak')

        req_json = request.get_json()
        rating = req_json['rating']

        mood = Mood(user_id=user_id, rating=rating)

        # Persist to database
        db.session.add(mood)
        db.session.commit()

        return dict({"message": "Mood created"}), 201

    except AttributeError:
        return dict({"message": "Error creating mood rating"}), 500


@mood_bp.route('/mood', methods=['GET'])
@login_required
@as_json
def get_user_moods():
    """Create a new mood entry for the currently logged in user"""
    try:
        user_id = current_user.get_id()
        if user_id is None:
            raise AttributeError("No current user id")

        moods = Mood.query.filter(User.id == user_id).order_by(
            User.created_on).all()
        data = Mood.list_items(moods)

        return dict(data=data), 200

    except AttributeError:
        return dict({"message": "Error getting user mood ratingS"}), 500
