"""Routes for interacting with users"""
from flask import Blueprint
from flask_json import as_json

from neuromood.database import db
from neuromood.models.user import User

user_bp = Blueprint('user', __name__, url_prefix='/users')


@user_bp.route('/', methods=['GET', 'POST'])
@as_json
def index():
    """Query all users"""
    users = User.query.all()
    results = User.list_items(users)
    return dict(data=results), 200


@user_bp.route('/<int:id>', methods=['GET'])
@as_json
def get_single_user(id):
    """Fetch a single user"""
    user = User.query.filter_by(id=id).first()

    if user:
        return dict(data=user), 200

    return dict({"message": "User not found"}), 404
