import sqlite3
import click

from flask import current_app, g
from flask.cli import with_appcontext

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def get_db():
    """Connect to database.  Connection is unique for each request
    and will be reused if called again.
    """
    if 'db' not in g:
        g.db = sqlite3.connect(current_app.config['DATABASE'],
                               detect_types=sqlite3.PARSE_DECLTYPES)

        g.db.row_factory = sqlite3.Row

    return g.db


def close_db(e=None):
    """If request if connected to the database, close it."""
    db = g.pop('db', None)

    if db is not None:
        db.close()


def init_db():
    """Clear existing data and create new tables."""
    db = get_db()

    with current_app.open_resource('schema.sql') as f:
        db.executescript(f.read().decode('utf-8'))


@click.command('init-db')
@with_appcontext
def init_db_cmd():
    """Clear existing data and create new tables."""
    init_db()
    click.echo('Initialized database')


def init_app(app):
    """Register database functions w/ Flask app.  Called via application
    factory.
    """
    app.teardown_appcontext(close_db)
    app.cli.add_command(init_db_cmd)
