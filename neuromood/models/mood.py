"""Database Model for `Moods`"""
from datetime import datetime

from neuromood.database import db, get_db


class Mood(db.Model):
    """Mood model"""
    __tablename__ = 'moods'

    id = db.Column(db.Integer, primary_key=True)

    user_id = db.Column(
        db.Integer,
        db.ForeignKey('users.id'),
        nullable=False,
    )

    rating = db.Column(db.Integer)

    created_on = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False,
        default=datetime.utcnow,
    )

    modified_on = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=True,
    )

    def list_items(self):
        query = f"SELECT id, user_id, rating, created_on " \
                f"FROM moods"
        results = get_db().execute(query).fetchall()
        result = [{
            column: row[i]
            for i, column in enumerate(results[0].keys())
        } for row in results]

        return result

    def __repr__(self):
        return '<Mood {}>'.format(self.id, self.user_id, self.rating,
                                  self.created_on)
