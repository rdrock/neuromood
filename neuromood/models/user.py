from datetime import datetime

from flask_login import UserMixin
from neuromood.database import db, get_db


class User(UserMixin, db.Model):
    """User Model"""
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)

    email = db.Column(
        db.String(50),
        nullable=False,
        unique=True,
    )

    password = db.Column(
        db.String(200),
        primary_key=False,
        unique=False,
        nullable=False,
    )

    streak = db.Column(db.Integer)

    created_on = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=False,
        default=datetime.utcnow,
    )

    modified_on = db.Column(
        db.DateTime,
        index=False,
        unique=False,
        nullable=True,
    )

    def list_items(self, where_clause=""):
        query = f"SELECT id, email, streak " \
                f"FROM users"
        results = get_db().execute(query).fetchall()
        result = [{
            column: row[i]
            for i, column in enumerate(results[0].keys())
        } for row in results]

        return result

    def __json__(self):
        return '<User {}>'.format(self.id, self.email, self.streak,
                                  self.created_on, self.modified_on)

    def __repr__(self):
        return '<User {}>'.format(self.id, self.email, self.streak,
                                  self.created_on, self.modified_on)
