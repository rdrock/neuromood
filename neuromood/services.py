import datetime

from neuromood.database import db, get_db
from neuromood.models.mood import Mood
from neuromood.models.user import User


def format_query_results(query_result):
    """Converts database results from `execute()`"""
    result = [{
        column: row[i]
        for i, column in enumerate(query_result[0].keys())
    } for row in query_result]
    # print('Format Result >>> ', result)
    return result


def is_within_steak_time_frame(date):
    # TODO: Replace with 24 datetime
    delta = date - datetime.datetime(2020, 5, 2)

    if delta < datetime.timedelta(hours=24):
        return True
    return False


def reset_user_streak(user_id):
    """Resets a user's current streak"""
    query = f"UPDATE streak " \
            f"SET streak = 0 " \
            f"FROM users " \
            f"WHERE id == {user_id}"
    print('Reset query >>> ', query)
    # results = get_db().execute(query).fetchall()
    # result = format_query_results(results)
    result = []
    return result


def list_mood_insertions(user_id):
    query = f"SELECT id, user_id, rating, created_on, modified_on " \
            f"FROM users WHERE user_id == {user_id}"
    print('Query => ', query)
    results = get_db().execute(query).fetchall()
    result = format_query_results(results)
    return result


def get_last_insertion_date(user_id):
    query = f"SELECT mood.id mood_id, mood.user_id, mood.rating, " \
            f" mood.created_on mood_created_date, " \
            f" mood.modified_on mood_modified_date, " \
            f"user.id user_id, user.streak " \
            f"FROM moods mood " \
            f"INNER JOIN users user " \
            f"ON mood.user_id = user.id " \
            f"ORDER BY mood_created_date DESC " \
            f"LIMIT 1"
    results = get_db().execute(query).fetchall()
    result = format_query_results(results)
    return result
