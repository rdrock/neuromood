# Production Write-up

| Concept       | Description                                                                              |
| ------------- | ---------------------------------------------------------------------------------------- |
| Documentation | Add more robust documentation to cover the entire API                                    |
| Versioning    | Using the git tagging feature following [Semantic Versioning 2.0.0](https://semver.org/) |
| Environments  | Isolated testing, staging, and production environments                                   |
| Error Tracing | Tools to help trace errors for assisting in debugging                                    |
| Metrics       | General analytics on our application and database usage                                  |
| CI/ CD        | Integrate more CI/CD jobs, include manual deployments for production, and setup testing  |
| Code Review   | Merge Requests approved to go in the `master` branch should be peer-reviewed             |

## Documentation

The API should be well documented. I prefer OpenAPI.

## Versioning

The API will have a version identifier using [Semantic Versioning 2.0.0](https://semver.org/). The versions will correlate to
a [GitLab Tag](https://docs.gitlab.com/ee/user/project/releases/).

Tagging documentation: [Tagging Basics](https://git-scm.com/book/en/v2/Git-Basics-Tagging).

## DevOps

For a production environment, I like to have several tools for monitoring the application's health.

- Sentry: Error tracing and monitoring.
- Grafana: Analytics on various telemetric points.

### CI/ CD

Nothing extremely fancy, but more for sanity checks:

- Dependency installation
- Building application
- Testing
- Deploy to a staging environment IFF (if-and-only-if) pipeline passes
- Configure manual deployment job for production. Conventially on the `master` branch

## Testing

Test driven development is a crucial step in assisting engineers stay sane. Tests should include:

- Database initialization
- Querying data
- POSTing data
- Any services

## Database

Improvements to the database should include:

- Create an `EXTENSTION` for UUID generation
- Update `modified_on` columns automatically
- Add indexes
