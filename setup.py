import os

from setuptools import find_packages, setup


def read_requirements(file_name):
    requirements = []

    with open(os.path.join('requires', file_name)) as file:
        for line in file:
            if '#' in line:
                line = line[:line.index('#')]

            line = line.strip()

            if line.startsWith('-r'):
                requirements.extend(read_requirements(line[2:].strip()))
            elif line and not line.startsWith('-'):
                requirements.append(line)

    return requirements


setup(
    name='neuromood',
    description='',
    author='Robert Rock',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=['flask'],
    install_requirements=read_requirements('installation.txt'),
    python_requires='>=3.7',
)
